import { Module } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'
import { ScheduleModule } from '@nestjs/schedule'

import { StatusModule } from './status/status.module'
import { ServerModule } from './server/server.module'
import { PrometheusModule } from './prometheus/prometheus.module'
import { MetricModule } from './metrics/metric.module'
import { AutoDownModule } from './autostop/autostop.module'

@Module({
  imports: [
    ScheduleModule.forRoot(),
    ConfigModule.forRoot({ isGlobal: true, envFilePath: 'config.env' }),
    ServerModule,
    StatusModule,
    PrometheusModule,
    MetricModule,
    AutoDownModule,
  ],
})
export class AppModule {}
