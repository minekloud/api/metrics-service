import { Socket } from 'socket.io'

export class UserStatusRequest {
  client: Socket
  userId: number
  serversId: number[]
}
