import { ApiProperty } from '@nestjs/swagger'

export enum Status {
  HEALTHY = 'healthy',
  UNHEALTHY = 'unhealthy',
}

export class StatusReponse {
  @ApiProperty()
  serverId: number
  @ApiProperty()
  timestamp: Date
  @ApiProperty()
  status: Status
}
