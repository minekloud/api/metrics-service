import { Injectable } from '@nestjs/common'
import { Interval } from '@nestjs/schedule'
import { Socket } from 'socket.io'
import { UserStatusRequest } from './user-status-request'
import { StatusReponse, Status } from './status-response'
import { StatusMetricService } from '../metrics/services/server-status-metric.service'
@Injectable()
export class StatusService {
  private usersRequestsToFetch: UserStatusRequest[] = []

  constructor(private readonly statusMetricService: StatusMetricService) {}

  addUserRequestToFetch(newUserRequest: UserStatusRequest) {
    console.log(`New request from client ${newUserRequest.client.id} with userId ${newUserRequest.userId} for server ${newUserRequest.serversId}`)
    this.usersRequestsToFetch.push(newUserRequest)
    this.fetchServerStatus(newUserRequest)
  }

  removeUserRequestToFetch(client: Socket) {
    this.usersRequestsToFetch = this.usersRequestsToFetch.filter(
      (userRequest) => userRequest.client.id != client.client.id
    )
  }

  @Interval(5000)
  handleIntervalFetchStatus() {
    if(this.usersRequestsToFetch.length == 0){
      return
    }
    console.log('Interval handle: update servers list')
    this.usersRequestsToFetch.forEach(async (userRequest) => {
      this.fetchServerStatus(userRequest)
    })
  }

  private async fetchServerStatus(userRequest: UserStatusRequest) {
    const responses: StatusReponse[] = []
    await Promise.all(
      userRequest.serversId.map(async (serverId) => {
        const metricStatus = await this.statusMetricService.getLiveStatusMetric(
          String(serverId)
        )
        if (!metricStatus) {
          return
        }
        responses.push({
          serverId,
          status: metricStatus.value as Status,
          timestamp: metricStatus.timestamp,
        })
      })
    )
    console.log(`Send msg to ${userRequest.client.id} - userId ${userRequest.userId} - ${userRequest.serversId} - res: ${responses.length}`)
    userRequest.client.emit('server-status-metric', responses)
  }
}
