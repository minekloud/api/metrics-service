import { Module } from '@nestjs/common'
import { MetricModule } from 'src/metrics/metric.module'
import { ServerModule } from 'src/server/server.module'
import { StatusGateway } from './status.gateway'
import { StatusService } from './status.service'
import { AuthentificationModule } from '../authentification/authentification.module'

@Module({
  imports: [MetricModule, ServerModule, AuthentificationModule],
  providers: [StatusGateway, StatusService],
  exports: [StatusService],
})
export class StatusModule {}
