import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { ServerService } from '../server/server.service'

@Injectable()
export class ServerPermission implements CanActivate {

  private internalBasicAuth: string

  constructor(private readonly serverService: ServerService, private readonly configService: ConfigService) {
    this.internalBasicAuth = Buffer.from(this.configService.get<string>('INTERNAL_BASIC_AUTH_TOKEN')).toString('base64')
  }

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest(  )
    if(request.headers['authorization'] == `Basic ${this.internalBasicAuth}`) {
      return true
    }

    if (!request.headers['authorization'] || !request.params.id) {
      return false
    }

    try {
      const payloadBase64 = request.headers['authorization'].split('.')[1]
      const userId = JSON.parse(
        Buffer.from(payloadBase64, 'base64').toString('utf8')
      ).sub
      const requestServerId = request.params.id
      if (!(await this.serverService.isUserAllowed(userId, requestServerId))) {
        return false
      }
    } catch (error) {
      console.log(
        `ServerPermission - Guard : error while parse authorization header`
      )
      return false
    }

    return true
  }
}
