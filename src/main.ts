import { ValidationPipe } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { NestFactory } from '@nestjs/core'
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger'
import { version } from '../package.json'
import { AppModule } from './app.module'

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {cors: true})
  const config: ConfigService = app.get(ConfigService)
  app.useGlobalPipes(new ValidationPipe({ transform: true }))
  app.setGlobalPrefix(config.get<string>('GLOBAL_PREFIX') || '/api/metrics')

  const options = new DocumentBuilder()
    .setTitle('Metrics Service')
    .setDescription('The metrics service API description')
    .setVersion(version)
    // .addBearerAuth()
    .build()

  const document = SwaggerModule.createDocument(app, options)
  SwaggerModule.setup('api/docs/metrics', app, document)

  await app.listen(config.get<number>('APP_PORT'))
}
bootstrap()
