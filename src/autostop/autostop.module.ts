import { Module } from '@nestjs/common'
import { ServerModule } from 'src/server/server.module'
import { AutoStopService } from './autostop.service'

@Module({
  imports: [ServerModule],
  providers: [AutoStopService],
  exports: [AutoStopService],
})
export class AutoDownModule {}
