import { Injectable } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { Interval } from '@nestjs/schedule'
import { ServerStatus } from 'src/models/server-status'
import { ServerService } from 'src/server/server.service'
import { Server } from '../models/server'

@Injectable()
export class AutoStopService {
  private autoStopDelay: number
  private serverAutoStopMap: Map<number, Date> = new Map()

  constructor(
    private readonly serverService: ServerService,
    private readonly configService: ConfigService
  ) {
    this.autoStopDelay = this.configService.get<number>(
      'AUTO_STOP_DELAY'
    )
  }

  autoStop(server: Server) {
    const newDate = new Date()

    if (!this.serverAutoStopMap.has(server.id)) {
      this.serverAutoStopMap.set(server.id, newDate)
      return
    }

    const serverTimestamp = this.serverAutoStopMap.get(server.id)
    const diffInSeconds = (newDate.getTime() - serverTimestamp.getTime()) / 1000

    if (diffInSeconds < this.autoStopDelay) {
      console.log(`Server with ${server.id} don't have to be poweroff`)
      return
    }

    console.log(`Server with ID ${server.id} will be poweroff`)
    this.serverService.stopServer(server.id)
  }

  @Interval(Number(process.env.AUTO_STOP_INTERVAL) || 10000)
  async handleIntervalAutoStop() {
    console.log('Interval handle: autoStop')

    const servers = await this.serverService.getServers()
    if (!servers) {
      return
    }

    // Remove servers: offline, no autostop, no players online
    servers
      .filter(
        (server) =>
          server.autoStop == false ||
          server.status == ServerStatus.OFF ||
          server.players.length > 0
      )
      .forEach((srv) => {
        this.serverAutoStopMap.delete(srv.id)
      })

    // Execute autoStop if servers: online, autostop, players online
    servers
      .filter(
        (server) =>
          server.autoStop == true &&
          server.status == ServerStatus.ON &&
          server.players.length == 0
      )
      .forEach((server) => {
        this.autoStop(server)
      })
  }
}
