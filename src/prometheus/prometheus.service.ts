import { Injectable, HttpService } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { MetricResponse } from './metric'
import { MetricQuery } from '../metrics/metric-query'

@Injectable()
export class PrometheusService {
  private readonly baseUrl: string
  private readonly instantMetricEndpoint: string = '/query'
  private readonly rangeMetricEndpoint: string = '/query_range'

  constructor(
    private readonly configService: ConfigService,
    private readonly httpService: HttpService
  ) {
    this.baseUrl = this.configService.get<string>('PROMETHEUS_SERICE_URL')
  }

  async getInstantMetric(query: string): Promise<MetricResponse> {
    const response = await this.fetchPrometheusApi(
      `${this.instantMetricEndpoint}?query=${query}`
    )
    if (!response || response.data.result.length == 0) {
      return null
    }

    return {
      timestamp: response.data.result[0].value[0],
      value: response.data.result[0].value[1],
    }
  }

  async getRangeMetric(
    query: string,
    { start, end, offset, unit }: MetricQuery
  ): Promise<MetricResponse[]> {
    const response = await this.fetchPrometheusApi(
      `${this.rangeMetricEndpoint}?query=${query}&start=${Number(
        start
      )}&end=${Number(end)}&step=${offset}${unit}`
    )
    if (!response || response.data.result.length == 0) {
      return []
    }

    const result = response.data.result
      .map((metric) => metric.values)
      .flat()
      .map((value) => {
        return {
          timestamp: value[0],
          value: value[1],
        }
      })
    return result
  }

  private async fetchPrometheusApi(query: string) {
    console.log(`Request API Prometheus : ${this.baseUrl}${query}`)
    const result = await this.httpService
      .get(this.baseUrl + query)
      .toPromise()
      .then((res) => {
        return res.data
      })
      .catch((error) => {
        console.log(`Error when querying the prometheus API: ${query}`)
        // TODO: return exception !
        return null
      })
    return result
  }
}
