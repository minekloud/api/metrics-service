import { Injectable, HttpService } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'

@Injectable()
export class AuthService {
  private _baseURL: string

  constructor(
    private httpService: HttpService,
    private configService: ConfigService
  ) {
    this._baseURL = this.configService.get<string>('AUTH_SERVICE_URL')
  }

  public async isJwtValid(token: string): Promise<boolean> {
    const headersRequest = {
      Authorization: `Bearer ${token}`,
    }
    const result = await this.httpService
      .get(this._baseURL, { headers: headersRequest })
      .toPromise()
      .then((res) => {
        if (res.status == 200) {
          return true
        } else {
          console.log(
            `Error while validatin jwt with auth service, token: ${token}`
          )
          return false
        }
      })
      .catch((error) => {
        console.log(
          `Error while validatin jwt with auth service, token: ${token}`
        )
        return false
      })

    return result
  }
}
