import { Injectable } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { MetricQuery } from '../metric-query'
import { Status } from '../../status/status-response'
import { MetricResponse } from '../../prometheus/metric'
import { PrometheusService } from '../../prometheus/prometheus.service'

@Injectable()
export class StatusMetricService {
  private expressionStatus: string
  private kubernetesMinecraftContainerName: string

  constructor(
    private readonly prometheusService: PrometheusService,
    private readonly configService: ConfigService
  ) {
    this.expressionStatus = this.configService.get<string>(
      'PROM_EXPRESSION_MINECRAFT_STATUS'
    )
    this.kubernetesMinecraftContainerName = this.configService.get<string>(
      'K8S_MINECRAFT_CONTAINER_NAME'
    )
  }

  async getLiveStatusMetric(serverId: string): Promise<MetricResponse> {
    const metric = await this.prometheusService.getInstantMetric(
      `${this.expressionStatus}{service="server-${serverId}-${this.kubernetesMinecraftContainerName}"}`
    )
    if (!metric) {
      return null
    }

    return {
      timestamp: metric.timestamp,
      value: Number(metric.value) == 1 ? Status.HEALTHY : Status.UNHEALTHY,
    }
  }

  async getRangeStatusMetric(
    serverId: string,
    query: MetricQuery
  ): Promise<MetricResponse[]> {
    const metrics = await this.prometheusService.getRangeMetric(
      `${this.expressionStatus}{service="server-${serverId}-${this.kubernetesMinecraftContainerName}"}`,
      query
    )
    return metrics.map((metric) => {
      return {
        timestamp: metric.timestamp,
        value: Number(metric.value) == 1 ? Status.HEALTHY : Status.UNHEALTHY,
      }
    })
  }
}
