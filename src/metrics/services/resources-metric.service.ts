import { Injectable } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { MetricQuery } from '../metric-query'
import { MetricResponse } from '../../prometheus/metric'
import { PrometheusService } from '../../prometheus/prometheus.service'

@Injectable()
export class ResourcesMetricService {
  private expressionCpu: string
  private expressionRam: string
  private kubernetesMinecraftContainerName: string

  constructor(
    private readonly prometheusService: PrometheusService,
    private readonly configService: ConfigService
  ) {
    this.expressionCpu = this.configService.get<string>(
      'PROM_EXPRESSION_CPU_USAGE'
    )
    this.expressionRam = this.configService.get<string>(
      'PROM_EXPRESSION_RAM_USAGE'
    )
    this.kubernetesMinecraftContainerName = this.configService.get<string>(
      'K8S_MINECRAFT_CONTAINER_NAME'
    )
  }
  async getLiveCpuMetric(serverId: string): Promise<MetricResponse> {
    const expression = `(sum(node_namespace_pod_container:${this.expressionCpu}:sum_rate{container="${this.kubernetesMinecraftContainerName}", pod=~"server-${serverId}-${this.kubernetesMinecraftContainerName}-.*"}) / sum(kube_pod_container_resource_limits_cpu_cores{namespace="default", container="${this.kubernetesMinecraftContainerName}", pod=~"server-${serverId}-${this.kubernetesMinecraftContainerName}-.*"}))*100`
    return this.prometheusService.getInstantMetric(expression)
  }

  async getRangeCpuMetric(
    serverId: string,
    query: MetricQuery
  ): Promise<MetricResponse[]> {
    const expression = `(sum(node_namespace_pod_container:${this.expressionCpu}:sum_rate{container="${this.kubernetesMinecraftContainerName}", pod=~"server-${serverId}-${this.kubernetesMinecraftContainerName}-.*"}) / sum(kube_pod_container_resource_limits_cpu_cores{namespace="default", container="${this.kubernetesMinecraftContainerName}", pod=~"server-${serverId}-${this.kubernetesMinecraftContainerName}-.*"}))*100`
    return this.prometheusService.getRangeMetric(expression, query)
  }

  async getLiveRamMetric(serverId: string): Promise<MetricResponse> {
    const expression = `sum(${this.expressionRam}{container="${this.kubernetesMinecraftContainerName}",pod=~"server-${serverId}-${this.kubernetesMinecraftContainerName}-.*",image!=""})by(container)`
    return this.prometheusService.getInstantMetric(expression)
  }

  async getRangeRamMetric(
    serverId: string,
    query: MetricQuery
  ): Promise<MetricResponse[]> {
    const expression = `sum(${this.expressionRam}{container="${this.kubernetesMinecraftContainerName}",pod=~"server-${serverId}-${this.kubernetesMinecraftContainerName}-.*",image!=""})by(container)`
    return this.prometheusService.getRangeMetric(expression, query)
  }
}
