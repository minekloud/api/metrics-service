import { Injectable, HttpService } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { Server } from 'src/models/server'
import { Player } from 'src/models/player'
import { ServerStatus } from 'src/models/server-status'

@Injectable()
export class ServerService {
  private _baseURL: string

  constructor(
    private httpService: HttpService,
    private configService: ConfigService
  ) {
    this._baseURL = this.configService.get<string>('SERVERS_SERVICE_URL')
  }

  async isUserAllowed(userId: number, serverId: number): Promise<boolean> {
    const server = await this.getServerById(serverId)
    if (server == null || server.idUser != userId) {
      return false
    }

    return true
  }

  private async getServerById(idServeur: number) {
    const result = await this.httpService
      .get(this._baseURL + '/' + idServeur)
      .toPromise()
      .then((res) => {
        return res.data
      })
      .catch((error) => {
        console.log(
          `Error when retrieving the server with the id: ${idServeur}`
        )
        //console.log(error)
        return null
      })
    return result
  }

  public async getServers(): Promise<Server[]> {
    const result = await this.httpService
      .get(this._baseURL)
      .toPromise()
      .then((res) => {
        return res.data
      })
      .catch((error) => {
        console.log(`Error when retreiving all servers: ${error}`)
        return null
      })
    return result
  }

  public async stopServer(serverId: number) {
    this.httpService
      .patch(`${this._baseURL}/${serverId}`, {
        status: ServerStatus.OFF,
      })
      .toPromise()
      .catch((error) => {
        console.log(`Error stop server with id ${serverId} : ${error}`)
      })
  }
}
