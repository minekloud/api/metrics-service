import { ServerStatus } from './server-status'
import { Player } from './player'

export interface Server {
  id: number
  status: ServerStatus
  autoStop: boolean
  players: Player[]
}
